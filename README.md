## Backstory
When my wife and I got married in 2016 we enjoyed a low-key wedding and had the family take pictures. My father managed the aggregation and transfer of the photos via a panicked frenzy of iCloud downloading. What we finally had on our hands was a massive collection of photos with an uncomfortably high number of duplicates

For the sake of good digital housekeeping the duplicates had to go. I tackled this problem with a simple script using python, an md5 hash and an sqlite table... I like playing with sqlite. When the script ran it generated the md5 hash for each video and image and then stored that along with the file path into the database. Finding the duplicate files at that point was a matter of looking up duplicate hashes in the table and then preparing a deletion list which would include all but one of each set of duplicates.

I was able to eliminate about 1.5Gigs of duplicate files. Don’t worry, I had a backup of the original mess in case something when wrong. I also made the final step of generating the deletion list interactive so that I had an opportunity to choose which duplicates got deleted. The nice thing about this method was that it didn’t care what type of file it was handling. I could see myself cleaning up the script and using it again later for another task.

## Usage
NOTE: The filepaths are currently hard coded.

I'm pretty sure you would do this:
1. run `find_duplicates.py > deletion_list.txt`
2. move the files to a deletable folder with `move_files.sh` this gives you a chance to review the deletions

## Direction for Improvement
#### Usability
This script could be made into a command line tool. Consider set of commands like: "dff scan <path>", "dff extract <duplicates>"

#### Health and Robustness
Write some tests!