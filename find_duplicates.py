import hashlib
import sqlite3
from os import listdir
from os.path import isfile, join

search_path = '''/path/to/duplicate/files/folder'''

#convert search_path to list of files
files = [f for f in listdir(search_path) if isfile(join(search_path, f))]


# initialize db
conn = sqlite3.connect(':memory:')
db = conn.cursor()
db.execute('''
    CREATE TABLE files 
    (filepath, hash)
    ''')

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

count = 0
for file in files:
    count += 1

    #calculate hash
    filepath = search_path + "/" + file
    hash = md5(filepath)

    #create database
    db.execute(
    '''
    INSERT INTO files(
        filepath,
        hash
        )
    VALUES (
        :filepath,
        :hash
    )
    ''',
    {
        "filepath": filepath,
        "hash": hash
    })

    #print filepath, hash
#print count

#print duplicates
statement = '''
    SELECT filepath, hash, COUNT(hash)
    FROM files
    GROUP BY hash
    HAVING COUNT(hash) > 1
'''
recordset = db.execute(statement)
duplicate_hashes = []
for i in recordset:
    duplicate_hashes.append( i[1] )

deletion_list = []

statement2 = '''
    SELECT filepath, hash
    FROM files
    WHERE hash=:hash
'''
for h in duplicate_hashes:
    recordset2 = db.execute(statement2, {'hash':h})
    duplicate_files = []
    for record in recordset2:
        duplicate_files.append({'path':record[0], 'hash':record[1]})

    option = 1
    for file in duplicate_files:
        print option, file['path'], file['hash']
        option += 1

    keep = int(raw_input())
    keep -= 1

    del duplicate_files[keep]
    
    for file in duplicate_files:
        deletion_list.append(file['path'])

print 'deletion list ---------------------------------------------------------'
for i in deletion_list:
    print i
