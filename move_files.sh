#!/bin/bash
# FILE: move_files.sh
# DESCRIPTION: Reads the contents of a duplicates-list (identified redundant files) and moves them
# to a designated folder.
cat myfile | while read foo ; do
    mv "$foo" Users/david/Desktop/move\ duplicates/.
done < deletion_list.txt
